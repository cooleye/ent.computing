﻿using UnityEngine;
using System.Collections;

public class GameSettings : MonoBehaviour
{
    public int mapSize = 5;
    public int players = 2;
    public float speed = 1.0f;

    public int[] playerPositions;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    void Start()
    {

    }
}
