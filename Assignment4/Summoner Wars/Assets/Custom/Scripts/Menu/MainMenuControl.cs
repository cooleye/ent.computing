﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenuControl : MonoBehaviour
{	
    public void QuickStartGame()
    {
        Debug.Log("Quick Start Game");
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }

    public void OpenCustomGameSettings()
    {
        Debug.Log("Open Custom Game Settings");
    }

    public void CloseCustomGameSettings()
    {
        Debug.Log("Close Custom Game Settings");
    }

    public void StartCustomGame()
    {
        Debug.Log("Start Custom Game");
    }

    public void ExitGame()
    {
        Debug.Log("Exit Game");
        Application.Quit();
    }
}
