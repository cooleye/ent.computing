﻿using UnityEngine;
using System.Collections;



public class Gameplay : MonoBehaviour
{
    public GameObject playerPrefab = null;
    public GameObject mapPrefab = null;

    private GameSettings settings = null;
    private Map map = null;
    private GameObject[] playerObjects;
    private PlayerControl[] players;

    // Use this for initialization
    void Start()
    {
        // Get game settings
        settings = GameObject.Find("GameSettings").GetComponent<GameSettings>();
        if (settings == null)
            return;

        // Create map
        map = Instantiate(mapPrefab).GetComponent<Map>();

        // Create player arrays
        playerObjects = new GameObject[settings.players];
        players = new PlayerControl[settings.players];

        // Create players
        for (int i = 0; i < settings.players; ++i)
        {
            // Instantiate player
            playerObjects[i] = Instantiate(playerPrefab);
            players[i] = playerObjects[i].GetComponent<PlayerControl>();

            // Set position
            Map.Position coord = map.GetStartPosition(i);            
            Vector3 pos = map.ToWorldPosition(coord);
            players[i].SetPosition(pos);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
