﻿using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public float moveSpeed = 0.2f;      // Speed for horizontal and vertical movement
    public float scrollSpeed = 0.2f;    // Speed for zooming in and out
    public float rotateSpeed = 1.0f;    // Speed for rotating
    
	void Update ()
    {
        // Get input
        float horizontal = Input.GetAxis("Horizontal") * moveSpeed;
        float vertical = Input.GetAxis("Vertical") * moveSpeed;
        float zoom = Input.GetAxis("Zoom") * scrollSpeed;
        float rotate = Input.GetAxis("Rotate") * rotateSpeed;

        // Move camera
        Vector3 move = transform.TransformDirection(horizontal, 0.0f, vertical);
        transform.Translate(move.x, 0.0f, move.z, Space.World);

        // Zoom
        transform.Translate(0.0f, 0.0f, zoom, Space.Self);

        // Rotate
        transform.Rotate(0.0f, rotate, 0.0f, Space.World);
    }
}
