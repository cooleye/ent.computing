﻿using UnityEngine;
using System.Collections;

public class Map : MonoBehaviour
{
    public struct Position
    {
        public Position(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int x;
        public int y;
    }

    public Material swamp = null;   // Swamp material
    public Material grass = null;   // Grass material
    public Material stone = null;   // Stone material
    public Material well = null;    // Well material
    
    // Tile array
    private Tile[][] tiles;
    private GameSettings settings = null;

    // Use this for initialization
    void Awake()
    {
        Debug.Log("Create map");

        // Get game settings
        settings = GameObject.Find("GameSettings").GetComponent<GameSettings>();
        if (settings == null)
        {
            Debug.Log("Creating map failed");
            return;
        }
            

        // Calculate number of columns
        int diameter = 2 * settings.mapSize - 1;
        tiles = new Tile[diameter][];

        // Calculate number of tiles
        for (int x = 0, i = settings.mapSize - diameter; x < diameter; ++x, ++i)
        {
            // Calculate number of rows
            int h = diameter - Mathf.Abs(i);
            tiles[x] = new Tile[h];
        }
        
        // Columns
        for (int x = 0, i = settings.mapSize - diameter; x < diameter; ++x, ++i)
        {
            // Rows
            for (int y = 0; y < tiles[x].Length; ++y)
            {
                // Convert array index to map coordinates
                Position pos = new Position(x, y);
                ToRelativePosition(ref pos);
                
                // Create tile
                tiles[x][y] = CreateTile(ref pos);
            }
        }

        Debug.Log("Map successfully created");
    }

    Tile CreateTile(ref Position pos)
    {
        if (pos.x == 0 && pos.y == 0)
        {
            return new WellTile(this, pos.x, pos.y);
        }

        if (pos.x == 0 || pos.y == 0 || pos.x == -pos.y)
        {
            return new GrassTile(this, pos.x, pos.y);
        }

        if (pos.x == pos.y)
        {
            return new StoneTile(this, pos.x, pos.y);
        }

        if (Mathf.Sign(pos.x) != Mathf.Sign(pos.y))
        {
            bool placeable = true;
            

            for (int i = -1; i < 2; ++i)
            {
                for (int j = -1; j < 2; ++j)
                {
                    if (i != j)
                    {
                        Position p = new Position(pos.x + i, pos.y + j);

                        ToAbsolutePosition(ref p);

                        if (p.x >= 0 && p.x < tiles.Length && p.y >= 0 && p.y < tiles[p.x].Length)
                        {

                            Tile tile = tiles[p.x][p.y];
                            if (tile != null && tile.type == Tile.Type.WELL)
                            {
                                placeable = false;
                            }
                        }
                    }
                }
            }

            if (placeable)
            {
                return new WellTile(this, pos.x, pos.y);
            }
        }

        // Create tile
        return new SwampTile(this, pos.x, pos.y);
    }
    
    // Converts tile array indices to map coordinates
    public void ToRelativePosition(ref Position pos)
    {
        // Shift lowest y-element higher (depending on position)
        if (pos.x >= settings.mapSize)
        {
            pos.y += 2 * settings.mapSize - 1 - tiles[pos.x].Length;
        }

        // Convert to map coordinates (relative to center)
        pos.x -= pos.y;
        pos.y -= settings.mapSize - 1;
    }

    // Converts map coordinates to tile array indices
    public void ToAbsolutePosition(ref Position pos)
    {
        // Convert to array coordinates
        pos.y += settings.mapSize - 1;
        pos.x += pos.y;

        // Shift lowest y-element to 0
        if (pos.x >= settings.mapSize)
        {
            pos.y -= 2 * settings.mapSize - 1 - tiles[pos.x].Length;
        }
    }

    public Vector3 ToWorldPosition(Position pos)
    {
        return tiles[pos.x][pos.y].GetWorldPosition();
    }

    public Position GetStartPosition(int player)
    {
        Position pos = new Position();

        switch(player)
        {
            case 0:
                pos.x = 0;
                pos.y = 2 - settings.mapSize;
                break;

            case 1:
                pos.x = 0;
                pos.y = settings.mapSize - 2;
                break;

            case 2:
                pos.x = settings.mapSize - 2;
                pos.y = 2 - settings.mapSize;
                break;

            case 3:
                pos.x = 2 - settings.mapSize;
                pos.y = settings.mapSize - 2;
                break;

            case 4:
                pos.x = settings.mapSize - 2;
                pos.y = 0;
                break;

            case 5:
                pos.x = 2 - settings.mapSize;
                pos.y = 0;
                break;
        }

        ToAbsolutePosition(ref pos);
        return pos;
    }
}
