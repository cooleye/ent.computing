﻿using UnityEngine;
using System.Collections;

public abstract class Tile
{
    public enum Type
    {
        GRASS,
        WELL,
        SWAMP,
        STONE,
        OTHER
    }
    
    // Constants
    public const float SCALE = 1.0f;    // Scales the tiles
    public const float OFFSET = 1.02f;  // Scales the space between the tiles

    // A game object where the tile instances are created from
    protected static GameObject gameObjectTemplate = null;

    // The game object instance of the Tile
    protected GameObject gameObject;

    public Type type;


    // Constructor
    protected Tile (GameObject parent, int x, int y)
    {
        // Create new game object, if this is the first constructor call for this class
        if (!gameObjectTemplate)
        {
            gameObjectTemplate = GenerateGameObject();
            gameObject = gameObjectTemplate;
        }

        // Use existing game object to create instance otherwise
        else
        {
            gameObject = Object.Instantiate(gameObjectTemplate);
        }
        
        // Calculate object position
        float px = OFFSET * Mathf.Sqrt(3 * SCALE) * (x + 0.5f * y);
        float pz = OFFSET * SCALE * -1.5f * y;

        // Set properties
        gameObject.transform.parent = parent.transform;
        gameObject.transform.position = new Vector3(px, 0.0f, pz);
        gameObject.name = "Tile (" + x + ", " + y + ")";
    }
    
    protected GameObject GenerateGameObject()
    {
        // Calculate lengths for vertex positions
        float scale_half = SCALE / 2;
        float scale_vertical = Mathf.Sqrt(3 * SCALE) / 2;

        // Create vertices
        Vector3[] vertices = new Vector3[7];
        vertices[0] = new Vector3(-scale_vertical, 0.0f, scale_half);   // Top Left
        vertices[1] = new Vector3(0.0f, 0.0f, SCALE);                   // Top
        vertices[2] = new Vector3(scale_vertical, 0.0f, scale_half);    // Top Right
        vertices[3] = new Vector3(scale_vertical, 0.0f, -scale_half);   // Bottom Right
        vertices[4] = new Vector3(0.0f, 0.0f, -SCALE);                  // Bottom
        vertices[5] = new Vector3(-scale_vertical, 0.0f, -scale_half);  // Bottom Left
        vertices[6] = new Vector3(0.0f, 0.0f, 0.0f);                    // Center

        // Generate triangles
        int[] triangles = new int[18];
        for (int i = 0; i < 6; ++i)
        {
            triangles[i * 3] = 6;
            triangles[i * 3 + 1] = i;
            triangles[i * 3 + 2] = (i + 1) % 6;
        }

        // Generate UV
        Vector2[] uv = new Vector2[7];
        for (int i = 0; i < 7; ++i)
        {
            uv[i] = new Vector2(vertices[i].x, vertices[i].z) / SCALE;
        }

        // Create mesh
        Mesh mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uv;
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        mesh.Optimize();

        // Create game object
        GameObject gameObject = new GameObject();
        gameObject.AddComponent<MeshRenderer>();
        gameObject.AddComponent<MeshFilter>().mesh = mesh;
        return gameObject;
    }

    public Vector3 GetWorldPosition()
    {
        return gameObject.transform.position;
    }
}

public class SwampTile : Tile
{
    public SwampTile(Map parent, int x, int y) : base (parent.gameObject, x, y)
    {
        // Set swamp material
        gameObject.GetComponent<Renderer>().material = parent.swamp;

        // Set type
        type = Type.SWAMP;
    }
}

public class GrassTile : Tile
{
    public GrassTile(Map parent, int x, int y) : base(parent.gameObject, x, y)
    {
        // Set grass material
        gameObject.GetComponent<Renderer>().material = parent.grass;

        // Set type
        type = Type.GRASS;
    }
}

public class StoneTile : Tile
{
    public StoneTile(Map parent, int x, int y) : base(parent.gameObject, x, y)
    {
        // Set stone material
        gameObject.GetComponent<Renderer>().material = parent.stone;

        // Set type
        type = Type.STONE;
    }
}

public class WellTile : Tile
{
    public WellTile(Map parent, int x, int y) : base(parent.gameObject, x, y)
    {
        // Set well material
        gameObject.GetComponent<Renderer>().material = parent.well;

        // Set type
        type = Type.WELL;
    }
}