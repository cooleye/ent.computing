% class
% -----------------------------------------------------------------------
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{assignment}
\LoadClass[11pt,a4paper,oneside]{article}

% packages
% -----------------------------------------------------------------------
\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}
\usepackage{fancyhdr}
\usepackage{parskip}
\usepackage{tabularx}
\usepackage{listings}
\usepackage{booktabs}
\usepackage{amsmath, amssymb}
\usepackage[amsmath,amsthm,thmmarks]{ntheorem}
\usepackage{array}
\usepackage{color}
\usepackage{rotating}
\usepackage{gensymb}
\usepackage{epstopdf}
\usepackage{tikz}
\usetikzlibrary{arrows,positioning}

% commands
% -----------------------------------------------------------------------
\newcommand{\thistitle}{Entertainment Computing}
\newcommand{\studentA}{Kevin Marnholz}
\newcommand{\studentB}{Kameran Qasim Elias Abdi}

% layout
% -----------------------------------------------------------------------
\setlength{\textwidth}{16cm}
\setlength{\topmargin}{-1cm}
\setlength{\evensidemargin}{0cm}
\setlength{\oddsidemargin}{0cm}
\setlength{\textheight}{24cm}
\setlength{\parskip}{1ex}
\setlength{\headheight}{14px}
\setlength{\parindent}{0ex}
\pagestyle{fancy}
\thispagestyle{empty}

% macros
% -----------------------------------------------------------------------
\definecolor{codebg}{rgb}{0.95,0.95,0.95}
\definecolor{codelines}{rgb}{0.3,0.3,0.3}
\lstset{
	%frame=L,
	xleftmargin=20px,
	%numbers=left,
	%language={Java},
	firstline=1,
	breaklines = true,
	backgroundcolor=\color{codebg},
	%numberstyle=\tiny\color{codelines},
	basicstyle=\ttfamily
}

\tikzset{
	dot/.style={
		solid,circle,draw=black,very thick
	},
	end/.style={
		solid,rectangle,draw=black,very thick,
	}
}

% table: \dline
\newcommand\dline{
	\hline\rule[0.85\dp\@arstrutbox]{0pt}{0.85\ht\@arstrutbox}
}
% table: \tline
\newcommand\tline{
	\hline\hline\rule[0.85\dp\@arstrutbox]{0pt}{0.85\ht\@arstrutbox}
}    
    
% maketitle
% -----------------------------------------------------------------------
\renewcommand{\maketitle}[2]{\@maketitle{#1}{#2}{}{}{}{}{}{}}
\renewcommand{\@maketitle}[8]{

    % title
    % -----------------------------------------------------------------------
	\begin{tabular}{p{10cm}l}
		\LARGE{\textbf{{\thistitle}}} & 
		\begin{tabular}{c}
			\studentA \\ \studentB
		\end{tabular}\centering
	\end{tabular}\\
	\rule{\linewidth}{1pt} \\[7px]
	\begin{tabular}{p{11cm}p{3cm}}
		\LARGE{Assignment {#2}} & \centering{#1}
	\end{tabular}
	
	% headings
	% -----------------------------------------------------------------------
	\fancyhead{}
	\fancyhead[R]{\thistitle}
	\fancyhead[L]{\studentA, \studentB}

% end
}\endinput